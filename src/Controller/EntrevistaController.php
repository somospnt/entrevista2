<?php

namespace App\Controller;

use App\Service\EntrevistaService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EntrevistaController extends AbstractController {
        
    /**
      * @Route("/entrevistas")
      */
    function entrevistas(EntrevistaService $entrevistaService) {
        return $this->render('entrevistas.html.twig', [
            "entrevistas" => $entrevistaService->buscarTodas()
            ]);
    }
}
