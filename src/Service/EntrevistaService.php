<?php

namespace App\Service;

use App\Repository\EntrevistaRepository;

class EntrevistaService {

    private $entrevistaRepository;

    public function __construct(EntrevistaRepository $entrevistaRepository) {
        $this->entrevistaRepository = $entrevistaRepository;
    }

    function buscarTodas() {
        return $this->entrevistaRepository->findAll();
    }

}
