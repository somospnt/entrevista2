<?php

namespace App\Repository;

use App\Entity\Entrevista;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Entrevista|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entrevista|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entrevista[]    findAll()
 * @method Entrevista[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntrevistaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entrevista::class);
    }

    // /**
    //  * @return Entrevista[] Returns an array of Entrevista objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Entrevista
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
