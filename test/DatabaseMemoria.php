<?php

use Addiks\PHPSQL\PDO\PDO;

class DatabaseMemoria {

    function crearEntrevistas() {
# create a new database in memory
        $pdo = new PDO('inmemory:some_example_database');

        $pdo->query("
    CREATE TABLE entrevistas( 
        id INT PRIMARY KEY AUTO_INCREMENT,
        nombre_entrevistado VARCHAR(45) NOT NULL,
        fecha DATETIME NOT NULL
    )
");

        $pdo->query("
    INSERT INTO entrevistas 
        (id, nombre_entrevistado, fecha) 
    VALUES
        (1, 'pepeEnMemoria1', '2019-10-04'),
        (2, 'pepeEnMemoria2', '2019-10-04'),
        (3, 'pepeEnMemoria3', '2019-10-04'),
        (4, 'pepeEnMemoria4', '2019-10-04')
");

        $result = $pdo->query("SELECT * FROM entrevistas");

        $rows = $result->fetchAll();

        $entrevistas = [];

        foreach ($rows as $row) {
            array_push($entrevistas, new Entrevista($row["id"], $row["nombre_entrevistado"], new DateTime($row["fecha"])));
        }

        return $entrevistas;
    }

}
